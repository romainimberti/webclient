firebase.auth().onAuthStateChanged(function (user) {
	if (user) {
		window.location.href = "home.html";
	}
});

function signUp() {
	const signUpError = document.getElementById("signUpError");
	signUpError.innerHTML = "";

	var email = document.getElementById("signUpEmail").value;
	var password = document.getElementById("signUpPassword").value;
	var confirmPassword = document.getElementById("signUpConfirmPassword").value;

	if (password !== confirmPassword) {
		signUpError.innerHTML = "The passwords should match.";
		return;
	}

	firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
		var errorMessage = error.message;
		signUpError.innerHTML = errorMessage;
	});
}

function signIn() {
	const signInError = document.getElementById("signInError");
	signInError.innerHTML = "";

	var email = document.getElementById("signInEmail").value;
	var password = document.getElementById("signInPassword").value;

	firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
		var errorMessage = error.message;
		signInError.innerHTML = errorMessage;
	});
}

function showSignIn() {
	document.getElementById("signInForm").style.display = "block";
	document.getElementById("signUpForm").style.display = "none";

	document.getElementById("signInCover").classList.remove("bg-primary");
	document.getElementById("signUpCover").classList.add("bg-primary");
}

function showSignUp() {
	document.getElementById("signUpForm").style.display = "block";
	document.getElementById("signInForm").style.display = "none";

	document.getElementById("signUpCover").classList.remove("bg-primary");
	document.getElementById("signInCover").classList.add("bg-primary");
}

window.addEventListener("load", function () {
	document.getElementById("signUp").addEventListener("click", signUp);
	document.getElementById("signIn").addEventListener("click", signIn);
	document.getElementById("showSignIn").addEventListener("click", showSignIn);
	document.getElementById("showSignUp").addEventListener("click", showSignUp);
});